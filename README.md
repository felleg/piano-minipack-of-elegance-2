# How to add this pack to Etterna/StepMania:
Download (or clone) this pack on your computer. Extract the `piano-minipack-of-elegance-2` folder under your Etterna (or StepMania) `Songs` folder.

# We still need your help!
The songs and the steps contained in this pack are final. However, this project is still missing some graphics (i.e. banners and backgrounds). Please visit this link to see which files need graphics: https://goo.gl/uKJLG6

- Please send your work to `pianominipack2@gmail.com` as an attachment
- You can send me a PM at http://www.flashflashrevolution.com/profile/MarioNintendo/
- ... merge requests are also accepted, of course!
